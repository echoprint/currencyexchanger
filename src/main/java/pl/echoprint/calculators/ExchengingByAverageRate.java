package pl.echoprint.calculators;

import pl.echoprint.sources.AverageRatesTable;

import java.util.List;

public interface ExchengingByAverageRate {

    double calculatePlnToCurrencyOnAverageRate(List<AverageRatesTable> averageRates);
}
