package pl.echoprint.calculators;

import pl.echoprint.sources.ArchivTable;

import java.util.List;

public interface ExchangingByTradingRate {

    double calculatePlnToCurrencyByTradingDayRate(List<ArchivTable> tradingRates);



}
