package pl.echoprint.calculators;

import pl.echoprint.sources.ArchivTable;
import pl.echoprint.sources.RateArchivTable;

import java.util.List;


public enum CurrencyExchangerByTradingRateImpl implements ExchangingByTradingRate {

    USD {
        @Override
        public double calculatePlnToCurrencyByTradingDayRate(final List<ArchivTable> tradingRates) {
            double monthAgoResult = 0.00;
            for (ArchivTable c : tradingRates) {
                for (RateArchivTable tr : c.getRateArchivTable()) {
                    if (tr.getCode().contains("USD")) {
                        monthAgoResult = AmountPln.AMOUNT_IN_PLN / tr.getAsk();
                    }
                }
            }
            return monthAgoResult;
        }
    },
    EUR {
        @Override
        public double calculatePlnToCurrencyByTradingDayRate(final List<ArchivTable> tradingRates) {
            double monthAgoResult = 0.00;
            for (ArchivTable c : tradingRates) {
                for (RateArchivTable tr : c.getRateArchivTable()) {
                    if (tr.getCode().contains("EUR")) {
                        monthAgoResult = AmountPln.AMOUNT_IN_PLN / tr.getAsk();
                    }
                }
            }
            return monthAgoResult;
        }
    },
    CHF {
        @Override
        public double calculatePlnToCurrencyByTradingDayRate(final List<ArchivTable> tradingRates) {
            double archivResult = 0.00;
            for (ArchivTable c : tradingRates) {
                for (RateArchivTable tr : c.getRateArchivTable()) {
                    if (tr.getCode().contains("CHF")) {
                        archivResult = AmountPln.AMOUNT_IN_PLN / tr.getAsk();
                    }
                }
            }
            return archivResult;
        }
    },
    GBP {
        @Override
        public double calculatePlnToCurrencyByTradingDayRate(final List<ArchivTable> tradingRates) {
            double archivResult = 0.00;
            for (ArchivTable c : tradingRates) {
                for (RateArchivTable tr : c.getRateArchivTable()) {
                    if (tr.getCode().contains("GBP")) {
                        archivResult = AmountPln.AMOUNT_IN_PLN / tr.getAsk();
                    }
                }
            }
            return archivResult;
        }
    }
}
