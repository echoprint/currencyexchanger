package pl.echoprint.calculators;

import pl.echoprint.sources.AverageRatesTable;
import pl.echoprint.sources.Rate;

import java.util.List;

public enum CurrencyExchangerByAverageRateImpl implements ExchengingByAverageRate {

    USD {
        @Override
        public double calculatePlnToCurrencyOnAverageRate(List<AverageRatesTable> averageRates) {
            double result = 0.00;
            for (AverageRatesTable w : averageRates) {
                for (Rate r : w.getRates()) {
                    if (r.getCode().contains("USD")) {
                        result = AmountPln.AMOUNT_IN_PLN / r.getMid();
                    }
                }
            }
            return result;
        }
    },
    EUR {
        @Override
        public double calculatePlnToCurrencyOnAverageRate(List<AverageRatesTable> averageRates) {
            double result = 0.00;
            for (AverageRatesTable w : averageRates) {
                for (Rate r : w.getRates()) {
                    if (r.getCode().contains("EUR")) {
                        result = AmountPln.AMOUNT_IN_PLN / r.getMid();
                    }
                }
            }
            return result;
        }
    },
    CHF {
        @Override
        public double calculatePlnToCurrencyOnAverageRate(List<AverageRatesTable> averageRates) {
            double result = 0.00;
            for (AverageRatesTable w : averageRates) {
                for (Rate r : w.getRates()) {
                    if (r.getCode().contains("CHF")) {
                        result = AmountPln.AMOUNT_IN_PLN / r.getMid();
                    }
                }
            }
            return result;
        }
    },
    GBP {
        @Override
        public double calculatePlnToCurrencyOnAverageRate(List<AverageRatesTable> averageRates) {
            double result = 0.00;
            for (AverageRatesTable w : averageRates) {
                for (Rate r : w.getRates()) {
                    if (r.getCode().contains("GBP")) {
                        result = AmountPln.AMOUNT_IN_PLN / r.getMid();
                    }
                }
            }
            return result;
        }
    }
}