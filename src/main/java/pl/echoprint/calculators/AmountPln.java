package pl.echoprint.calculators;

import java.util.Scanner;

import static java.lang.Double.parseDouble;

public class AmountPln {
    static double AMOUNT_IN_PLN;

    public double getAmountPln() {
        System.out.println("Give the amount of PLN to convert");
        try {
            final Scanner scanner = new Scanner(System.in);
            AMOUNT_IN_PLN = parseDouble(scanner.nextLine());
            System.out.printf("%s %.2f %s", "input amount: ", AMOUNT_IN_PLN, " PLN");

        } catch (Exception e) {
            System.out.println("Wrong format of data. Only numbers are accepted." + e);
        }
        return AMOUNT_IN_PLN;
    }

    public double getAMOUNT_IN_PLN() {
        return AMOUNT_IN_PLN;
    }
}
