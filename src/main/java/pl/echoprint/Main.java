package pl.echoprint;


import pl.echoprint.calculators.AmountPln;
import pl.echoprint.calculators.CurrencyExchangerByAverageRateImpl;
import pl.echoprint.calculators.CurrencyExchangerByTradingRateImpl;
import pl.echoprint.populators.AverageRatesPopulator;
import pl.echoprint.populators.TradingResults;
import pl.echoprint.sources.ArchivTable;
import pl.echoprint.sources.AverageRatesTable;

import java.io.IOException;
import java.util.List;


public class Main {


    public static void main(String[] args) throws IOException {

        ConnectionToUrl connectionToUrl = new ConnectionToUrl();
        AmountPln amount = new AmountPln();
        amount.getAmountPln();
        drawLine();

        List<AverageRatesTable> currentAverageRates = connectionToUrl.connectToAverageRatesTable();
        System.out.print("Current average rates on date: ");
        AverageRatesPopulator averageRatesPopulator = new AverageRatesPopulator();
        averageRatesPopulator.getCurrencyAverageRates(currentAverageRates);
        System.out.println();

        System.out.println(amount.getAMOUNT_IN_PLN() + " PLN calculated to currency by average rate on last published rates");
        for (CurrencyExchangerByAverageRateImpl c : CurrencyExchangerByAverageRateImpl.values()) {
            System.out.printf("%s  %.2f\n", c, c.calculatePlnToCurrencyOnAverageRate(currentAverageRates));
        }
        drawLine();

        List<ArchivTable> tradeRates = connectionToUrl.connectToArchivTable();
        System.out.print("Trade rates from date: ");
        TradingResults tradingResults = new TradingResults();
        tradingResults.populateLastTradeRates(tradeRates);
        System.out.println();

        System.out.println(amount.getAMOUNT_IN_PLN() + " PLN calculated to currency by last trading rate");
        getTradingResultsList(tradeRates);
        drawLine();

        List<ArchivTable> ratesMonthAgo = connectionToUrl.connectToArchivRatesTableAMonthAgo();
        System.out.print("Trading rates one month ago: ");
        TradingResults tr = new TradingResults();
        tr.populateArchivTradeRates(ratesMonthAgo);
        System.out.println();

        System.out.println(amount.getAMOUNT_IN_PLN() + " PLN calculated to currency by one month ago trading rate");
        getTradingResultsList(tradeRates);
        drawLine();
    }

    private static void getTradingResultsList(final List<ArchivTable> tradeRates) {
        for (CurrencyExchangerByTradingRateImpl c : CurrencyExchangerByTradingRateImpl.values()) {
            System.out.printf("%s  %.2f\n", c, c.calculatePlnToCurrencyByTradingDayRate(tradeRates));
        }
    }


    private static void drawLine() {
        System.out.println();
        System.out.println("--------");
        System.out.println();
    }
}
