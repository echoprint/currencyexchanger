package pl.echoprint;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import pl.echoprint.sources.ArchivTable;
import pl.echoprint.sources.AverageRatesTable;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ConnectionToUrl {

    private Gson gson = new Gson();

    public List<AverageRatesTable> connectToAverageRatesTable() throws IOException {
        String url_get = "http://api.nbp.pl/api/exchangerates/tables/a/?format=json";
        InputStream is = getInputStream(url_get);
        Scanner sc = new Scanner(is);
        String line = sc.nextLine();
        return
                gson.fromJson(line, new TypeToken<ArrayList<AverageRatesTable>>() {
                }.getType());
    }

    public List<ArchivTable> connectToArchivTable() throws IOException {
        String url_get = "http://api.nbp.pl/api/exchangerates/tables/c/?format=json";
        return getConnectionToArchivTable(url_get);
    }

    public List<ArchivTable> connectToArchivRatesTableAMonthAgo() throws IOException {
        String url_start = "http://api.nbp.pl/api/exchangerates/tables/c/";
        String middle = getTradingDateOneMonthEarlier().toString();
        String url_end = "/?format=json";
        String url_get = url_start + middle + url_end;
        return getConnectionToArchivTable(url_get);
    }

    public LocalDate getTradingDateOneMonthEarlier() {
        LocalDate today = LocalDate.now();
        LocalDate dayOfTheRatesPopulation;
        switch (today.minusMonths(1).getDayOfWeek()) {
            case SATURDAY:
                dayOfTheRatesPopulation = today.minusMonths(1).minusDays(1);
                break;
            case SUNDAY:
                dayOfTheRatesPopulation = today.minusMonths(1).minusDays(2);
                break;
            default:
                dayOfTheRatesPopulation = today.minusMonths(1);
        }
        return dayOfTheRatesPopulation;
    }

    private InputStream getInputStream(String url_get) throws IOException {
        URL url = new URL(url_get);
        URLConnection conn = url.openConnection();
        conn.addRequestProperty("user-agent", "Chrome");
        return conn.getInputStream();
    }

    private List<ArchivTable> getConnectionToArchivTable(String url_get) throws IOException {
        InputStream is = getInputStream(url_get);
        Scanner sc = new Scanner(is);
        String line = sc.nextLine();
        return
                gson.fromJson(line, new TypeToken<ArrayList<ArchivTable>>() {
                }.getType());
    }
}

