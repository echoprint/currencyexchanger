package pl.echoprint.populators;

import pl.echoprint.sources.ArchivTable;
import pl.echoprint.sources.RateArchivTable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TradingResults {


    public void populateLastTradeRates(List<ArchivTable> archivTradeRate) {
        getTradingRates(archivTradeRate);
    }

    public void populateArchivTradeRates(List<ArchivTable> tradeRateMonthAgo) {
        getTradingRates(tradeRateMonthAgo);
    }

    private void getTradingRates(List<ArchivTable> archivTradeRate) {
        for (ArchivTable c : archivTradeRate) {
            Map<String, Double> archivRates = new HashMap<>();
            System.out.println(c.getTradingDate());

            for (RateArchivTable ar : c.getRateArchivTable()) {
                switch (ar.getCode()) {
                    case "USD":
                        archivRates.put(ar.getCode(), ar.getAsk());
                    case "EUR":
                        archivRates.put(ar.getCode(), ar.getAsk());
                    case "CHF":
                        archivRates.put(ar.getCode(), ar.getAsk());
                    case "GBP":
                        archivRates.put(ar.getCode(), ar.getAsk());
                }

                for (Map.Entry<String, Double> chosenTradeRates : archivRates.entrySet()) {
                    String currencyCode = chosenTradeRates.getKey();
                    Double mid = chosenTradeRates.getValue();
                    System.out.println(currencyCode + ": " + mid);
                }
                archivRates.clear();
            }
        }
    }
}