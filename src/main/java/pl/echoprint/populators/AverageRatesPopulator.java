package pl.echoprint.populators;

import pl.echoprint.sources.AverageRatesTable;
import pl.echoprint.sources.Rate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AverageRatesPopulator {

    public void getCurrencyAverageRates(List<AverageRatesTable> lastAverage) {
        Map<String, Double> averageRates = new HashMap<>();
        for (AverageRatesTable k : lastAverage) {
            System.out.println(k.getEffectiveDate());

            for (Rate r : k.getRates())
                switch (r.getCode()) {
                    case "USD":
                        averageRates.put(r.getCode(), r.getMid());
                    case "EUR":
                        averageRates.put(r.getCode(), r.getMid());
                    case "CHF":
                        averageRates.put(r.getCode(), r.getMid());
                    case "GBP":
                        averageRates.put(r.getCode(), r.getMid());
                }

            for (Map.Entry<String, Double> chosenAverageRates : averageRates.entrySet()) {
                String currencyCode = chosenAverageRates.getKey();
                Double mid = chosenAverageRates.getValue();
                System.out.println(currencyCode + ": " + mid);
            }
            averageRates.clear();
        }
    }
}
