
package pl.echoprint.sources;

public class RateArchivTable {

    private String currency;
    private String code;
    private double bid;
    private double ask;

    public RateArchivTable(String currency, String code, double bid, double ask) {
        this.currency = currency;
        this.code = code;
        this.bid = bid;
        this.ask = ask;
    }

    public double getAsk() {
        return ask;
    }

    public String getCurrency() {
        return currency;
    }

    public String getCode() {
        return code;
    }
}
