
package pl.echoprint.sources;

public class Rate {

    private String code;
    private double mid;

    public Rate() {
    }

    public String getCode() {
        return code;
    }

    public double getMid() {
        return mid;
    }

    @Override
    public String toString() {
        return "Rate{" +
                "code='" + code + '\'' +
                ", mid=" + mid +
                '}';
    }
}
