
package pl.echoprint.sources;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class ArchivTable {

    private String table;
    private String no;
    private String tradingDate;
    private String effectiveDate;
    @SerializedName("rates")
    private List<RateArchivTable> rateArchivTable;

    public ArchivTable(String table, String no, String tradingDate, String effectiveDate, List<RateArchivTable> rateArchivTable) {
        this.table = table;
        this.no = no;
        this.tradingDate = tradingDate;
        this.effectiveDate = effectiveDate;
        this.rateArchivTable = rateArchivTable;
    }

    public String getTradingDate() {
        return tradingDate;
    }

    public List<RateArchivTable> getRateArchivTable() {
        return rateArchivTable;
    }


}
