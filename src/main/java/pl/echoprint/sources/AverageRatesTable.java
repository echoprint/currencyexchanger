
package pl.echoprint.sources;

import java.util.List;


public class AverageRatesTable {

    private String effectiveDate;
    private List<Rate> rates;

    public AverageRatesTable(String effectiveDate, List<Rate> rates) {
        this.effectiveDate = effectiveDate;
        this.rates = rates;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public List<Rate> getRates() {
        return rates;
    }

}
