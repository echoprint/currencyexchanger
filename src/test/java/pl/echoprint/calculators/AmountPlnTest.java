package pl.echoprint.calculators;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AmountPlnTest {

    private final AmountPln amount = new AmountPln();

    @Test
    void shouldReturnAmountInDouble() {
        String input = "1000";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        assertEquals(1000, amount.getAmountPln());
    }
}